/**
License:
	Boost Software License - Version 1.0 - August 17th, 2003

	Permission is hereby granted, free of charge, to any person or organization
	obtaining a copy of the software and accompanying documentation covered by
	this license (the "Software") to use, reproduce, display, distribute,
	execute, and transmit the Software, and to prepare derivative works of the
	Software, and to permit third-parties to whom the Software is furnished to
	do so, all subject to the following:
	
	The copyright notices in the Software and this entire statement, including
	the above license grant, this restriction and the following disclaimer,
	must be included in all copies of the Software, in whole or in part, and
	all derivative works of the Software, unless such copies or derivative
	works are solely in the form of machine-executable object code generated by
	a source language processor.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
	SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
	FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
	ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.

Authors:
	aermicioi
**/
module resources.config.resources;

import aermicioi.aedi_vibed;

import resources.config.primitives;
import controllers.worker;
import storage.rest_storage;
import storage.storage;
import storage.rest_storage;
import storage.caching_storage;
import storage.in_memory_storage;
import storage.model;
import storage.round_robin_storage;
import storage.mongo_storage;
import storage.mirror_storage;
import discovery.discovery;

import vibe.vibe : UDPConnection, listenUDP, URLRouter, RestInterfaceClient;
import std.datetime;

struct Arguments {
    ushort vibed__server__port = 8081;
    string[] vibed__server__bindAddresses = [];
    
    string kernel__discovery__listen__host = "127.0.0.100";
    ushort kernel__discovery__listen__port = 9000;
    string kernel__discovery__emit__host = "127.0.0.1";
    ushort kernel__discovery__emit__port = 9000;
    
    string mode = "leaf";
    string kernel__database__collection__worker = "mongo.worker";
    
    string leaf__cache__origin__host = "127.0.0.10";
    ushort leaf__cache__origin__port = 8081;
}

auto common(VibedContainer container) {
    container.registerInto!UDPConnection()
        .callback(
            (Locator!() locator) {
                return listenUDP(
                    locator.locate!ushort("kernel.discovery.listen.port"),
                    locator.locate!string("kernel.discovery.listen.host"),
                );
            }
        );
}

auto leaf(VibedContainer container) {
    import vibe.vibe : MongoClient, MongoCollection, connectMongoDB;
    
    container.registerInto!MongoClient("leaf")
        .callback(
            (Locator!() locator) {
                return connectMongoDB(
                    locator.locate!string("kernel.database.host"),
                    locator.locate!ushort("kernel.database.port"),
                );
            }
        );
        
    container.registerInto!MongoCollection("leaf")
        .callback(
            (Locator!() locator) {
                MongoClient client = locator.locate!MongoClient();
                return client.getCollection(
                    locator.locate!string("kernel.database.collection.worker")
                );
            }
        );
    
    container.register!(WorkerStorage, MongoWorkerStorage)("leaf")
        .set!"collection"(lref!MongoCollection);
        
    container.register!(WorkerRest, WorkerRestImpl)("leaf")
        .set!"storage"(lref!WorkerStorage)
        .rest(lref!URLRouter);
        
    container.registerInto!LocationEmitter("leaf")
        .set!"connection"(lref!UDPConnection)
        .set!"host"("kernel.discovery.emit.host".lref)
        .set!"port"("kernel.discovery.emit.port".lref)
        .set!"source"("source".lref)
        .timeable(dur!"seconds"(1), true);
        
    container.register!Location("source", "leaf")
        .set!"port"("vibed.server.port".lref)
        .set!"type"(StorageType.leaf);
}

auto maven(VibedContainer container) {
    
    container.register!(WorkerRest, XmlWorkerRestImplementation)("maven")
        .construct(lref!WorkerStorage)
        .route(lref!URLRouter);
    
    container.registerInto!(InMemoryWorkerStorage)("maven");
    container.register!(WorkerStorage, CachingWorkerStorage)("maven")
        .set!"decorated"(lref!RoundRobinWorkerStorage)
        .set!"cache"(lref!InMemoryWorkerStorage);
    
    container.registerInto!(RoundRobinWorkerStorage)("maven");
    container.registerInto!(LocationListener)("maven")
        .set!"connection"(lref!UDPConnection)
        .set!"add"(lref!AggregatingRestRegisterer)
        .runnable;
        
    container.registerInto!AggregatingRestRegisterer("maven")
        .set!"aggregator"(lref!RoundRobinWorkerStorage)
        .set!"acceptable"(StorageType.leaf)
        .set!"factory"(lref!RestWorkerStorageFactory);
        
    container.registerInto!(RestWorkerStorageFactory)("maven");
    
    container.registerInto!LocationEmitter("maven")
        .set!"connection"(lref!UDPConnection)
        .set!"host"("kernel.discovery.emit.host".lref)
        .set!"port"("kernel.discovery.emit.port".lref)
        .set!"source"("source".lref)
        .timeable(dur!"seconds"(1), true);
    
    container.register!Location("source", "maven")
        .set!"port"("vibed.server.port".lref)
        .set!"type"(StorageType.maven);
}

auto mirror(VibedContainer container) {
    
    container.register!(WorkerRest, WorkerRestImpl)("mirror")
        .set!"storage"(lref!WorkerStorage)
        .rest(lref!URLRouter);
    
    container.register!(WorkerStorage, MirrorStorage)("mirror");
    container.registerInto!(LocationListener)("mirror")
        .set!"connection"(lref!UDPConnection)
        .set!"add"(lref!SettingRestRegisterer)
        .set!"add"("caches".lref)
        .runnable;
        
    container.register!AggregatingRestRegisterer("caches", "mirror")
        .set!"aggregator"(lref!WorkerStorage)
        .set!"acceptable"(StorageType.cache)
        .set!"factory"(lref!RestWorkerStorageFactory);
        
    container.registerInto!(SettingRestRegisterer)("mirror")
        .set!"decorated"(lref!WorkerStorage)
        .set!"factory"(lref!RestWorkerStorageFactory)
        .set!"acceptable"(StorageType.maven);
        
    container.registerInto!(RestWorkerStorageFactory)("mirror");
    
    container.registerInto!LocationEmitter("mirror")
        .set!"connection"(lref!UDPConnection)
        .set!"host"("kernel.discovery.emit.host".lref)
        .set!"port"("kernel.discovery.emit.port".lref)
        .set!"source"("source".lref)
        .timeable(dur!"seconds"(1), true);
    
    container.register!Location("source", "mirror")
        .set!"port"("vibed.server.port".lref)
        .set!"type"(StorageType.mirror);
}

auto cache(VibedContainer container) {
    
    container.register!(WorkerRest, WorkerRestImpl)("cache")
        .set!"storage"(lref!WorkerStorage)
        .rest(lref!URLRouter);
    
    container.registerInto!InMemoryWorkerStorage("cache");
    container.register!(WorkerStorage, CachingWorkerStorage)("cache")
        .set!"cache"(lref!InMemoryWorkerStorage);
    
    container.registerInto!(LocationListener)("cache")
        .set!"connection"(lref!UDPConnection)
        .set!"add"(lref!SettingRestRegisterer)
        .runnable;
        
    container.registerInto!(SettingRestRegisterer)("cache")
        .set!"decorated"(lref!WorkerStorage)
        .set!"factory"(lref!RestWorkerStorageFactory)
        .set!"acceptable"(StorageType.maven);
        
    container.registerInto!(RestWorkerStorageFactory)("cache");
    
    container.registerInto!LocationEmitter("cache")
        .set!"connection"(lref!UDPConnection)
        .set!"host"("kernel.discovery.emit.host".lref)
        .set!"port"("kernel.discovery.emit.port".lref)
        .set!"source"("source".lref)
        .timeable(dur!"seconds"(1), true);
    
    container.register!Location("source", "cache")
        .set!"port"("vibed.server.port".lref)
        .set!"type"(StorageType.cache);
}

auto leafCache(VibedContainer container) {
    
    container.register!(WorkerRest, WorkerRestImpl)("leafCache")
        .set!"storage"(lref!WorkerStorage)
        .rest(lref!URLRouter);
    
    container.registerInto!InMemoryWorkerStorage("leafCache");
    container.register!(WorkerStorage, FullCachingWorkerStorage)("leafCache")
        .set!"cache"(lref!InMemoryWorkerStorage)
        .set!"decorated"(lref!RestWorkerStorage)
        .timeable(dur!"seconds"(10), true);
    
    container.registerInto!RestWorkerStorage("leafCache")
        .set!"client"(lref!(RestInterfaceClient!WorkerRest));
        
    container.registerInto!(RestInterfaceClient!WorkerRest)("leafCache")
        .callback(
            (Locator!() loc) {
                import std.conv;
                
                return new RestInterfaceClient!WorkerRest(
                    "http://" ~
                    loc.locate!string("leaf.cache.origin.host") ~
                    ":" ~
                    loc.locate!ushort("leaf.cache.origin.port").to!string  ~
                    "/"
                );
            }
        );
        
    container.registerInto!LocationEmitter("leafCache")
        .set!"connection"(lref!UDPConnection)
        .set!"host"("kernel.discovery.emit.host".lref)
        .set!"port"("kernel.discovery.emit.port".lref)
        .set!"source"("source".lref)
        .timeable(dur!"seconds"(1), true);
        
    container.register!Location("source", "leafCache")
        .set!"port"("vibed.server.port".lref)
        .set!"type"(StorageType.leaf);
}

auto parameters(VibedContainer container) {
    
    container.args!Arguments();
    container.envs!Arguments();
    
    import std.file : getcwd;
    container.register(getcwd(), "kernel.dir.root");
    container.register!string("kernel.dir.public")
        .append!string("kernel.dir.root".lref)
        .append!string("public/");
    container.register!string("kernel.dir.var")
        .append!string("kernel.dir.root".lref)
        .append!string("var/");
    container.register!string("kernel.dir.log")
        .append!string("kernel.dir.var".lref)
        .append!string("log/");
        
    container.register!ushort(27017, "kernel.database.port");
    container.register("127.0.0.1", "kernel.database.host");
}