import std.stdio;

import aermicioi.aedi;
import aermicioi.aedi_vibed;
import resources.config.resources;

void main(string[] args) {
    VibedContainer container = new VibedContainer(args);
    
    auto leaf = 
        (new SubscribableContainer!(SwitchableContainer!SingletonContainer)).decorated =
        (new SwitchableContainer!SingletonContainer).decorated = new SingletonContainer;
        
    auto maven = 
        (new SubscribableContainer!(SwitchableContainer!SingletonContainer)).decorated =
        (new SwitchableContainer!SingletonContainer).decorated = new SingletonContainer;
        
    auto mirror = 
        (new SubscribableContainer!(SwitchableContainer!SingletonContainer)).decorated =
        (new SwitchableContainer!SingletonContainer).decorated = new SingletonContainer;
        
    auto cache = 
        (new SubscribableContainer!(SwitchableContainer!SingletonContainer)).decorated =
        (new SwitchableContainer!SingletonContainer).decorated = new SingletonContainer;
    
    auto leafCache = 
        (new SubscribableContainer!(SwitchableContainer!SingletonContainer)).decorated =
        (new SwitchableContainer!SingletonContainer).decorated = new SingletonContainer;
        
    leaf.subscribe(
        ContainerInstantiationEventType.pre,
        () {
            if (container.locate!string("mode") == "leaf") {
                leaf.decorated.enabled = true;
            }
        }
    );
    
    maven.subscribe(
        ContainerInstantiationEventType.pre,
        () {
            if (container.locate!string("mode") == "maven") {
                maven.decorated.enabled = true;
            }
        }
    );
    
    mirror.subscribe(
        ContainerInstantiationEventType.pre,
        () {
            if (container.locate!string("mode") == "mirror") {
                mirror.decorated.enabled = true;
            }
        }
    );
    
    cache.subscribe(
        ContainerInstantiationEventType.pre,
        () {
            if (container.locate!string("mode") == "cache") {
                cache.decorated.enabled = true;
            }
        }
    );
    
    leafCache.subscribe(
        ContainerInstantiationEventType.pre,
        () {
            if (container.locate!string("mode") == "leafCache") {
                leafCache.decorated.enabled = true;
            }
        }
    );
    
    container.set(leaf, "leaf");
    container.set(maven, "maven");
    container.set(mirror, "mirror");
    container.set(cache, "cache");
    container.set(leafCache, "leafCache");
    container.common();
    container.leaf();
    container.maven();
    container.mirror();
    container.cache();
    container.leafCache();
    parameters(container);
    
    container.boot();
}