for i in `seq 9094 9104`; do ./pad --vibed.server.port=$i --mode=leaf & done
sleep 1;
./pad --vibed.server.port=8082 --mode=maven --kernel.discovery.listen.host=127.0.0.1 --kernel.discovery.listen.port=9000 --kernel.discovery.emit.host=127.0.0.2 --kernel.discovery.emit.port=9000 &
sleep 1;
./pad --vibed.server.port=8084 --vibed.server.bindAddresses="127.0.0.2" --mode=cache --kernel.discovery.listen.host=127.0.0.2 --kernel.discovery.listen.port=9000 --kernel.discovery.emit.host=127.0.0.2 --kernel.discovery.emit.port=9000 &
sleep 1;
./pad --vibed.server.port=8083 --vibed.server.bindAddresses="127.0.0.2" --mode=mirror --kernel.discovery.listen.host=127.0.0.2 --kernel.discovery.listen.port=9000 --kernel.discovery.emit.host=127.0.0.3 --kernel.discovery.emit.port=9000 &
sleep 1;