"# My project's README" 

# Implementation of a simplistic DDBS system.

This repository is a simple implementation of laboratory nr 3,4,5,6 in PAD course.

#Installation and usage

1. Download dmd compiler from (dlang.org)[dlang.org] and install it.
2. Navigate to Alexandru folder, and issue `dub build --arch=x86`.
3. Run in console simple.run.bat, and press enter after some seconds. The example implementation should be running.
4. Navigate to pad-lab-6 folder and run `Simple_client.py`. Result should be displayed after saving the data in db, and fetching it back.
5. Modify Simple_client.py, or use it in pythons REPL, to manually issue new requests.

**Thanks for previewing our application**

**pad-development-studio**